﻿using UnityEngine;
using System.Collections;

public class Pirate : MonoBehaviour {

	//Pirate Parameters
	public float VELOCITY_RANGE = 15;  // this divided by 2 is the Value to add or remove from VELOCITY_BISECTION
	public float VELOCITY_BISECTION = 30; // Average velocity at launch
	public float maxMagnitudeAllowed = 2000f; // If max magnitude is reached, boost from bounces are not multiplied
	public float minMagnitudeAllowed = 40f; // If magnitude is lower than this, pirate is drawn
	public float maxAngleAllowed = 50; // If angle is higher than this when hiting water, pirate is drawn
	public float coef_WaterBounceFriction = 1f; // Coeficien that affect Friction when hiting water, the higher it is, the more friction the pirate will have
	public float coef_ObstacleBounceY = 1f; // Coeficien that affect Velocity Y when hiting an obstacle, the higher it is, the higher the pirate will go
	public float coef_ObstacleBounceX = 1f; // Coeficien that affect Velocity X when hiting an obstacle, the higher it is, the faster the pirate will go

	//info
	public float currentMagnitude = 0;

	//state
	public bool isDrawn;

	//Description
	public string SkillsDescription;



	/**------------------------------NATIVES--------------------------------**/
	
		// Use this for initialization
	void Start () 
	{
		isDrawn = false;
	}
	
	// Update is called once per frame
	void Update () 
	{
		//print ("Angle: "+getAngle());

		//infos purpose
		currentMagnitude = getMagnitude ();

		if (transform.position.y < -10)
		{
			stopMotion();
			isDrawn = true;
		}
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		//print ("ho, collision!!!!!!");

		Vector3 currentVelocity = rigidbody2D.velocity;

		if (col.gameObject.tag == "ObstacleTag")
		{
			//print ("ho, pirate hit obstacle");
			Obstacle obstacle = col.gameObject.GetComponent<Obstacle>();
			currentVelocity.y = Mathf.Abs(currentVelocity.y);

			if(getMagnitude() < maxMagnitudeAllowed)
			{
				currentVelocity.y = currentVelocity.y * (obstacle.boostY * coef_ObstacleBounceY);
				currentVelocity.x = currentVelocity.x * (obstacle.boostX * coef_ObstacleBounceX);
			}

			//check if magnitude is high enough to bounce
			if(getMagnitude() < minMagnitudeAllowed)
			{
				stopMotion();
				isDrawn = true;
			}
			else
			rigidbody2D.velocity = currentVelocity;
		}

		else if (col.gameObject.tag == "WaterTag")
		{
			print ("ho, pirate hit water");


			//check if the angle when hiting the water is not to high to bounce
			if(isGoingDown() && getAngle() > maxAngleAllowed)
			{
				stopMotion();
				isDrawn = true;
			}

			//check if magnitude is high enough to bounce
			if(getMagnitude() < minMagnitudeAllowed)
			{
				stopMotion();
				isDrawn = true;
			}

			else
			{
				currentVelocity.y = Mathf.Abs(currentVelocity.y);
				currentVelocity = currentVelocity * (0.8F / coef_WaterBounceFriction);
				rigidbody2D.velocity = currentVelocity;
			}


		}



	}

	/**------------------------------PUBLICS--------------------------------**/

	//throw pirate
	public void launch(float _force, float _angle)
	{
		rigidbody2D.isKinematic = false;
		_force = VELOCITY_BISECTION + (_force * VELOCITY_RANGE / 2);
		Quaternion angle = Quaternion.AngleAxis (_angle, Vector3.forward);
		Vector3 direction = angle * Vector3.right;
		rigidbody2D.velocity = direction*_force;
		print ("ANGLE at launch: " + _angle);
		print ("FORCE at launch: " + _force);
	}

	/**
	 * Stop
	 * */
	public void stopMotion()
	{
		rigidbody2D.velocity = new Vector3(0,0,0);
		rigidbody2D.isKinematic = true;
	}

	/**
	 * Move To given position
	 * */
	public void moveTo(Vector3 position)
	{
		transform.position = position;
	}

	/**
	 * get current velocity
	 * */
	public float getMagnitude()
	{
		return rigidbody2D.velocity.sqrMagnitude;
	}

	/**
	 * get current angle in degree
	 * */
	public float getAngle()
	{
		float angle = Vector3.Angle(rigidbody2D.velocity, Vector3.right);
		return angle;
	}

	/**
	 * get current position
	 * */
	public Vector3 getPosition()
	{
		return transform.position;
	}

	/**
	 * Is going down or not
	 * */
	public bool isGoingDown()
	{
		Vector3 localVel = transform.InverseTransformDirection(rigidbody2D.velocity);
		return (localVel.y < 0);
	}

	/**
	 * Get string containing infos about current pirates
	 * Linear drag (Friction)
	 * Gravity Scale
	 * Min angle allowed
	 * Min magnitude allowed
	 * Max velocity at launch
	 * ...
	 * */
	public string getInfos()
	{
		float maxVel = VELOCITY_BISECTION + VELOCITY_RANGE / 2;
		float minVel = VELOCITY_BISECTION - VELOCITY_RANGE / 2;
		string str = "";
		str += "\n" + "Skills: \t" + SkillsDescription;

		str += "\n" + "Max Velocity at launch: \t" + maxVel;
		str += "\n" + "Min Velocity at launch: \t" + minVel;

		str += "\n" + "Base Friction: \t" + rigidbody2D.drag;
		str += "\n" + "Gravity scale: \t" + rigidbody2D.gravityScale;

		str += "\n" + "Water friction coef: \t" + coef_WaterBounceFriction;
		str += "\n" + "Boost Altitude coef: \t" + coef_ObstacleBounceY;
		str += "\n" + "Boost Speed coef: \t" + coef_ObstacleBounceX;

		str += "\n" + "Max angle allowed: \t" + maxAngleAllowed;
		str += "\n" + "Min magnitude allowed: \t" + minMagnitudeAllowed;
		return str;
	}


	/**------------------------------PRIVATES--------------------------------**/
}
