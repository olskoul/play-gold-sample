using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

/**
 * NOTES
 * 
 * **
 * Time.deltaTime: 
 * If you add or subtract to a value every frame chances are you should multiply with Time.deltaTime. 
 * When you multiply with Time.deltaTime you essentially express: 
 * I want to move this object 10 meters per second instead of 10 meters per frame.
 * **
 * 
 * 
 * */

public class ScreenGame : Screen
{
	
	//world related
	public GameObject [] clouds;
	public GameObject water1;
	public GameObject obstaclesContainer;
	public GameObject [] obstacles;
	public int totalObstacles;

	private float distanceMaxBetweenObstacle; //world unit
	private float distanceMinBetweenObstacle; //world unit

	//Privates
	Launcher launcher;
	GameObject water;
	//public GameObject [] worldObjects = new Arr;
	private List<GameObject> worldObjects = new List<GameObject>();

	Button btnLoadPirate;
	Button btnBegin;
	Button btnFreezeAngle;
	Button btnFreezeForce;
	Button BtnGenerateWorld;
	//Button btnRestart;

	bool isInitiating = true;
	
	protected override void Awake ()
	{
		base.Awake ();

		//Initialize variables
		launcher = GameObject.Find("Launcher").GetComponent<Launcher>();

		btnLoadPirate = GameObject.Find("BtnLoadPirate").GetComponent<Button>();
		btnBegin = GameObject.Find("BtnBegin").GetComponent<Button>();
		btnFreezeAngle = GameObject.Find("BtnFreezeAngle").GetComponent<Button>();
		btnFreezeForce = GameObject.Find("BtnFreezeForce").GetComponent<Button>();
		BtnGenerateWorld = GameObject.Find("BtnGenerateWorld").GetComponent<Button>();
		//btnRestart = GameObject.Find("BtnRestart").GetComponent<Button>();


		distanceMaxBetweenObstacle = 20f;
		distanceMinBetweenObstacle = 5f;
	}
	
	protected override void Start ()
	{
		disableAllBtns ();
		changeLauncherState ();
	}
	
	// Update is called once per frame
	protected override void Update ()
	{
	}
	
	/**------------------------------OVERRIDES--------------------------------**/
	
	// Init function to overriden
	protected override void init ()
	{
		parent = transform.gameObject;
	}
	
	public override void intro ()
	{
		base.intro ();

		//Generate world
		generateWorld ();
	}
	
	/**------------------------------PUBLICS--------------------------------**/

	public void btnClickHandler(GameObject btnInstance)
	{
		changeLauncherState ();
	}

	/**
	 * Change Launcher state
	 * Basically, thats the click that set the angle, set the power and then shoot
	 * */
	public void changeLauncherState ()
	{
		disableAllBtns ();

		if (isInitiating)
		{
			btnLoadPirate.interactable = true;
			isInitiating = false;
			return;
		}

		LauncherState state  = launcher.changeState ();

		switch (state)
		{
		case LauncherState.IDLE:
			btnLoadPirate.interactable = true;
			break;
		case LauncherState.LOAD_PIRATE:
			btnBegin.interactable = true;
			break;
			
		case LauncherState.CHOOSE_ANGLE:
			btnFreezeAngle.interactable = true;
			break;
			
		case LauncherState.CHOOSE_FORCE:
			btnFreezeForce.interactable = true;
			break;

		case LauncherState.SHOOT:
			//btnRestart.interactable = true;
			btnLoadPirate.interactable = true;
			break;
			
		case LauncherState.LAUNCHED:
			btnLoadPirate.interactable = true;
			//btnBegin.interactable = true;
			break;
		}
	}

	/**
	 * End Game
	 * */
	public void endGame ()
	{
		Debug.Log ("GameScreen : GAME ENDED");
		Invoke ("gameHasEnded", 2);
	}
	
	/**
	 * Reset Game
	 * */
	public void resetGame ()
	{
		Debug.Log ("GameScreen : RESET GAME");
	}
	
	


	public void generateWorld()
	{
		foreach(GameObject obj in worldObjects)
		{
			GameObject.Destroy(obj);
		}
		worldObjects = new List<GameObject> ();
		
		//Water
		water = Instantiate(water1) as GameObject;
		water.transform.parent = obstaclesContainer.transform;
		Vector3 waterPosition = water.transform.position;
		Bounds waterBounds = water.transform.Find ("WaterCollider").GetComponent<BoxCollider2D>().bounds;

		worldObjects.Add (water);

		//obstacles
		float currentPosX = 3;
		Vector3 position = new Vector3 ();

		for(int i = 0; i < totalObstacles; i++)
		{
			GameObject obstacle = Instantiate(obstacles[Random.Range(0,obstacles.Length)]) as GameObject;//
			GameObject cloud = Instantiate(clouds[0]) as GameObject;
			//obstacle.transform.Find("ObstacleCollidercurrentPosX").gameObject.GetComponent<Obstacle>().boost = Random.Range(100,200);
			float delta = (i==0)?0:Random.Range(distanceMinBetweenObstacle, distanceMaxBetweenObstacle);
			position.x =  currentPosX + delta;
			position.y = waterPosition.y;
			position.z = 10;
			obstacle.transform.position = position;

			position.y += Random.Range(3,10);
			position.z = Random.Range(-9,10);
			cloud.transform.position = position;
			//obstacle.transform.localScale *= Random.Range(1,10);

			cloud.transform.parent = obstaclesContainer.transform;
			obstacle.transform.parent = obstaclesContainer.transform;
			currentPosX = position.x;

			worldObjects.Add (cloud);
			worldObjects.Add (obstacle);
		}

		//Set Water length
		float waterScaleFactor = (currentPosX > 20)?currentPosX / waterBounds.size.x + 1:50;
		Vector3 waterScale = water.transform.localScale;
		water.transform.localScale = new Vector3(waterScaleFactor, waterScale.y, waterScale.z);


		//Set Depth
		Vector3 temp = launcher.transform.position;
		temp.z = -10.0f;
		launcher.transform.position = temp;


	}

	/**------------------------------PRIVATES--------------------------------**/

	/**
	 * Start game
	 * */
	private void startGame (int startTimeLeft)
	{
	}
	
	/**
	 * Start has ended
	 * */
	private void gameHasEnded ()
	{
		app.gameHasEnded ();
	}

	/**
	 * Disable all btn
	 * */
	private void disableAllBtns()
	{
		//disable all btns
		btnLoadPirate.interactable = false;
		btnBegin.interactable = false;
		btnFreezeAngle.interactable = false;
		btnFreezeAngle.interactable = false;
		btnFreezeForce.interactable = false;
		//btnShoot.interactable = false;
		//btnRestart.interactable = false;
	}

	

}
