﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//PUBLIC STATIC CONST
public enum LauncherState
{
	IDLE,
	LOAD_PIRATE,
	CHOOSE_ANGLE,
	CHOOSE_FORCE,
	SHOOT,
	LAUNCHED
}

public class Launcher : MonoBehaviour
{

	//public
	public LauncherState STATE;
	public GameObject [] pirates;
	public GameObject catapult;
	public float ANGLE;
	public float FORCE;
	public CameraController cameraController;
	public Text InfosText;

	//privates
	private App app;
	private GameObject arrow;
	private Pirate pirateScript;
	private int pirateIndex;
	private Vector3 initPos;
	private float cameraDelta;
	private float currentMagnitude;
	private float currentAngle;
	
	//Angle related
	private float indexAngle = 0f;
	private Quaternion rotation;
	private float ROTATION_SPEED = 5f;
	private float ANGLE_BISECTION = 45f;
	private float ANGLE_RANGE = 90f;

	
	//Velocity related
	private float indexVelocity = 0f;

	//Arrow scaling
	private float SCALE_SPEED = 5f;
	private float SCALE_BISECTION = 1f;
	private float SCALE_RANGE = 1f;
	private float arrowScale;

	/**------------------------------NATIVES--------------------------------**/

	void Awake ()
	{
		//ref
		app = GameObject.Find ("App").GetComponent<App>();
		arrow = GameObject.Find ("Arrow");
		catapult = GameObject.Find ("catapult1");
		cameraController = Camera.main.GetComponent<CameraController> ();

	}

	// Use this for initialization
	void Start ()
	{
		initPos = catapult.transform.position; // save catapult's intial position
		cameraDelta = cameraController.getPosition ().x - initPos.x;
		setup ();
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{
		if (STATE == LauncherState.LOAD_PIRATE)
		{

		}

		if (STATE == LauncherState.CHOOSE_ANGLE)
		{
			//calculate angle within a specified range
			indexAngle += Time.deltaTime * ROTATION_SPEED;
			ANGLE = ANGLE_BISECTION + (Mathf.Sin (indexAngle) * ANGLE_RANGE / 2);
			//print ("ANGLE: "+ANGLE);

			//update view
			updateViewAngle();
		}

		if (STATE == LauncherState.CHOOSE_FORCE)
		{
			//calculate Force 
			indexVelocity += Time.fixedDeltaTime * SCALE_SPEED;
			FORCE = Mathf.Sin (indexVelocity); //Value is between -1 and 1;
			//print ("FORCE: "+FORCE);
			
			//update view
			updateViewForce();
		}

		if (STATE == LauncherState.SHOOT)
		{
			/*pirate.rigidbody2D.isKinematic = false;
			float force = VELOCITY_BISECTION + (FORCE * VELOCITY_RANGE / 2);
			Vector3 dir = Quaternion.AngleAxis(ANGLE, Vector3.forward) * Vector3.right;
			pirate.rigidbody2D.velocity = dir*force;*/

			pirateScript.launch(FORCE,ANGLE);
			STATE = LauncherState.LAUNCHED;
		}

		if (STATE == LauncherState.LAUNCHED)
		{
			if(pirateScript.isDrawn)
				STATE = LauncherState.IDLE;
		}
	}

	/**------------------------------PUBLICS--------------------------------**/

	public LauncherState changeState()
	{
		switch (STATE)
		{
		case LauncherState.IDLE:
			STATE = LauncherState.LOAD_PIRATE;
			reload();
			break;
		case LauncherState.LOAD_PIRATE:
			STATE = LauncherState.CHOOSE_ANGLE;
			break;

		case LauncherState.CHOOSE_ANGLE:
			STATE = LauncherState.CHOOSE_FORCE;
			break;

		case LauncherState.CHOOSE_FORCE:
			STATE = LauncherState.SHOOT;
			break;
		
		case LauncherState.SHOOT:

			STATE = LauncherState.LAUNCHED;
			break;

		case LauncherState.LAUNCHED:
			if(pirateScript.isDrawn)
			STATE = LauncherState.IDLE;
			break;

		default:
			STATE = LauncherState.IDLE;
			break;
		}

		print ("STATE HAS CHANGED: "+STATE);
		return STATE;
	}

	/**
	 * Stat where pirate position is set to tmatch the catapult position
	 * Pirate variable may have chaged (public variable) so reseting refrences is needed.
	 * */
	public void reload()
	{


		//set piratescript ref
		pirateIndex ++;

		if (pirateIndex == pirates.Length)
		{
			setup ();
			return;
		}


		//bring catapult to last pirate position
		if(pirateIndex > 0)
			bringCatapult (cameraController.getPosition(), pirateIndex);

		print ("RELOAD: index = "+pirateIndex);
		pirateScript = pirates[pirateIndex].GetComponent<Pirate> ();

		//Change camera pirate to follow
		cameraController.setTarget(pirates [pirateIndex]);

		//stop pirate motion if needed
		pirateScript.stopMotion();

		//Mount pirate on capapult
		Vector3 startPosition = catapult.transform.position;
		startPosition.z += -0.1f;
		pirateScript.moveTo(startPosition);

		//update text
		InfosText.text = "Pirate " + (pirateIndex + 1) + "/" + pirates.Length;
		InfosText.text += pirateScript.getInfos ();

	}

	
	/**------------------------------PRIVATES--------------------------------**/
	/**
	 * Setup 
	 * */
	private void setup()
	{
		pirateIndex = -1;
		STATE = LauncherState.IDLE;
		bringCatapult (initPos, 0);
		InfosText.text = "";
	}

	/**
	 * Setup 
	 * */
	private void bringCatapult(Vector3 catapultPosition, int currentPirateIndex)
	{
		Vector3 newPosition = new Vector3 ();

		//catapult
		newPosition.x = (currentPirateIndex == 0) ? catapultPosition.x : catapultPosition.x;// - cameraDelta; //later could not fix the camera today
		newPosition.y = catapult.transform.position.y;
		newPosition.z = catapult.transform.position.z;
		catapult.transform.position = newPosition;

		//arrow (should be UI component, but did'nt have the time to set it up 
		newPosition.x = (currentPirateIndex == 0) ? catapultPosition.x : catapultPosition.x;// -cameraDelta;//later could not fix the camera today
		newPosition.y = arrow.transform.position.y;
		newPosition.z = arrow.transform.position.z;
		arrow.transform.position = newPosition;

		//position all pirates under catapult
		float posy = -1;
		for (int i=currentPirateIndex; i<pirates.Length; i++)
		{
			Pirate pirate = pirates[i].GetComponent<Pirate> ();
			Vector3 position = new Vector3 ();
			posy -= pirate.gameObject.renderer.bounds.size.y/2;
			
			position.z += initPos.z-0.1f;
			position.y = posy;
			position = position + initPos;
			pirate.moveTo(position);
			
			posy -= pirate.gameObject.renderer.bounds.size.y/2;

			pirate.isDrawn = false;
			
		}
	}

	/**
	 * Rotate Arrow based on current angle calculation
	 * */
	private void updateViewAngle()
	{
		rotation = Quaternion.Euler (new Vector3 (0, 0, ANGLE));
		arrow.transform.rotation = rotation;
	}

	/**
	 * Scale Arrow based on current Force calculation
	 * */
	private void updateViewForce()
	{
		arrowScale = SCALE_BISECTION + (FORCE * SCALE_RANGE / 2);
		arrow.transform.localScale = new Vector3 (arrowScale, 1, 1);
	}
}
