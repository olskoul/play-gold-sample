﻿using UnityEngine;
using System.Collections;


public class ScreenIntro : Screen {
	
	//Publics
	
	//Privates
	SimpleButton skipIntro;

	protected override void Awake () 
	{
		base.Awake ();

		//ref variable
		skipIntro = GameObject.Find ("btnSkip").GetComponent<SimpleButton>();
		skipIntro.callback = showNextScreen;
	}
	
	protected override void Start () 
	{
		
	}
	
	// Update is called once per frame
	protected override void Update () {
	}
	
	/**------------------------------OVERRIDES--------------------------------**/
	
	// Init function to overriden
	protected override void init () 
	{
		parent = transform.gameObject;
	}
	
	public override void intro()
	{
		base.intro ();
	}
	
	/**------------------------------PUBLICS--------------------------------**/
	

	
	/**------------------------------PRIVATES--------------------------------**/
	
	/**
	 * Start game
	 * */
	public void showNextScreen () 
	{
		app.introHasBeenSkipped ();
	}
	

	
	
	
}
