﻿using UnityEngine;
using System.Collections;

public class Screen : MonoBehaviour {
	
	protected App app;
	
	[HideInInspector]
	public GameObject parent;
	
	
	// Use this for initialization
	protected virtual void Awake () 
	{
		//ref to app script
		app = GameObject.Find ("App").GetComponent<App> ();	
		
		//init
		init ();
		
		//security
		if (parent == null)
			Debug.LogError ("init must be overriden in " + this.name);
	}
	
	// Use this for initialization
	protected virtual void Start () 
	{
		
	}
	
	// Update is called once per frame
	protected virtual void Update () {
		
	}
	
	/**------------------------------PUBLICS--------------------------------**/
	public virtual void intro()
	{
		parent.SetActive (true);
	}
	
	public virtual void outro()
	{
		
		parent.SetActive (false);
	}
	
	/**------------------------------PROTECTED--------------------------------**/
	
	// Init function to overriden
	protected virtual void init () 
	{
		parent = null;
	}
}
