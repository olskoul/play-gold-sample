﻿using UnityEngine;
using System.Collections;

public class App : MonoBehaviour {
	
	private Screen screenGame;
	//private Screen screenIntro;
	private Screen screenHowTo;
	private Screen screenEnd;

	// Use this for initialization
	void Awake () 
	{
		//Set framerate //ignored in the editor.
		Application.targetFrameRate = 60;

		//disable all screens
		screenGame = GameObject.Find ("screenGame").GetComponent<ScreenGame>();
		//screenIntro = GameObject.Find ("screenIntro").GetComponent<ScreenIntro>();
	}
	
	void Start () 
	{
		//Disable all screen
		screenGame.parent.SetActive (false);

		//Quick dirty start (Sprint 1)
		introHasBeenSkipped ();
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	/**------------------------------PUBLICS--------------------------------**/
	/*
	 * Get score
	 * */
	public int getScore()
	{
		return 0;
	}
	
	/*
	 * Intro skip handler
	 * Show How To Play screen
	 * */
	public void introHasBeenSkipped()
	{
		//screenIntro.outro ();
		//screenHowTo.intro ();
		playGame ();
	}
	
	/*
	 * How to play skip handler
	 * Show Game screen
	 * */
	public void howToPlayHasBeenSkipped()
	{
		screenHowTo.outro ();
		screenGame.intro ();
	}

	/**
	 * Game has ended
	 * */
	public void gameHasEnded()
	{
		screenGame.outro ();
		screenEnd.intro ();
	}

	/**
	 * Replay Game
	 * */
	public void playGame()
	{
		screenGame.intro ();
	}

	/**
	 * Replay Game
	 * */
	public void replayGame()
	{
		screenGame.intro ();
		//screenEnd.outro ();
	}

	/**
	 * Back to Intro screen
	 * */
	public void backToIntro()
	{
		//screenIntro.intro ();
		//screenEnd.outro ();
	}
	
	/**------------------------------PRIVATES--------------------------------**/
}
