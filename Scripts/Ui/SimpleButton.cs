﻿using UnityEngine;
using System.Collections;

public class SimpleButton : MonoBehaviour {

	private GameObject ON;
	private GameObject OFF;
	private AudioSource audioClick;

	public delegate void MyCallbackDelegate();
	public MyCallbackDelegate callback;

	// Use this for initialization
	void Start () 
	{
		//ref childs
		var children = GetComponentsInChildren<Transform>();
		foreach (var child in children)
		{
			if (child.name == "on")
				ON = child.gameObject;
			if (child.name == "off")
				OFF = child.gameObject;
		}

		//set default state
		setOffState();

		//init sound
		AudioSource[] aSources = GetComponents<AudioSource>();
		if(aSources.Length > 0)
			audioClick = aSources[0];
	}
	
	// Update is called once per frame
	void Update () 
	{


	}
	/**------------------------------PUBLICS--------------------------------**/

	public void enable(bool flag)
	{
		transform.gameObject.SetActive (flag);
	}

	/**------------------------------PRIVATES--------------------------------**/
	void OnMouseDown ()
	{
		setOnState();
		if(audioClick != null)
			audioClick.Play();
	}

	void OnMouseUp ()
	{
		setOffState();

		Invoke ("triggerCallBack", 0.3f);

	}
	
	void triggerCallBack () 
	{
		//call the callback
		callback();
		
	}

	void setOnState()
	{
		ON.SetActive(true);
		OFF.SetActive(!ON.activeInHierarchy);
	}

	void setOffState()
	{
		ON.SetActive(false);
		OFF.SetActive(!ON.activeInHierarchy);
	}
}
