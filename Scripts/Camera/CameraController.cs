﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CameraController : MonoBehaviour {

	public GameObject distanceText;
	public GameObject pirate;
	public Vector3 offset;

	private Vector3 piratePos;
	private bool followX;
	private bool followY;
	private float verticalLimit; // limit where camera start to move to follow the pirate
	private float verticalDelta; // distance between pirate start position and vertical limit
	private float horizontalLimit; // limit where camera start to move to follow the pirate
	private float horizontalDelta; // distance between pirate start position and horizontal limit


/**------------------------------NATIVES--------------------------------**/
	// Use this for initialization
	void Start () 
	{
		offset = transform.position;

		//Set vertical limit
		verticalLimit = Camera.main.ViewportToWorldPoint (new Vector3(0f,0.9f,0f)).y;
		verticalDelta = verticalLimit - transform.position.y;
		//set horizontal limit
		horizontalLimit = Camera.main.ViewportToWorldPoint (new Vector3(0.5f,0f,0f)).x;
		horizontalDelta = horizontalLimit - transform.position.x;


	}
	
	// Update is called once per frame
	void LateUpdate () 
	{
		//make the camera follow the pirate if activeInHierarchy
		if (pirate.activeInHierarchy )//&& !pirate.rigidbody2D.isKinematic
		{
			//pirate position in world
			piratePos = pirate.transform.position;

			//pirate position on screen
			Vector3 pirateScreenPos = Camera.main.WorldToScreenPoint(piratePos);
			//print("pirateScreenPos: "+pirateScreenPos);

			//Start following horizontally pirate if its pass the middleof the screen
			followX = (piratePos.x >= horizontalLimit);

			//Start following vertically pirate if its pass the middleof the screen
			followY = (piratePos.y >= verticalLimit); 

			//print ("pirate.y: "+pirate.transform.position.y);
			//print ("camera.y: "+transform.position.y);

			//position camera
			float cameraX = (followX)?(piratePos.x - horizontalDelta)*.999f:offset.x;
			float cameraY = (followY)?(piratePos.y - verticalDelta)*.999f:offset.y;
			Vector3 newCameraPosition = new Vector3(cameraX,cameraY,offset.z);
			transform.position = newCameraPosition + offset;

			//update Distance text
			distanceText.GetComponent<Text>().text = transform.position.x.ToString() + "m";

		}
		//else
			//transform.position = offset;
	}

	// Update is called once per frame
	void Update () 
	{
	}

	/**------------------------------PUBLIC--------------------------------**/
	public void setTarget(GameObject target)
	{
		pirate = target;

	}

	public void moveTo(Vector3 position)
	{
		transform.position = position;
	}

	public Vector3 getPosition()
	{
		return transform.position;
	}

	/**------------------------------PRIVATE--------------------------------**/
	private void adjustPosition()
	{

	}
}
